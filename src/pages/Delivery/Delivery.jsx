import React from "react";
import "./delivery.scss";

function Delivery() {
  return (
    <div className="delivery-wrapper">
      <div className="delivery">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum
          facere at accusamus odit consequuntur neque, repudiandae nulla aliquam
          obcaecati porro? Asperiores repellendus eligendi qui nihil non,
          adipisci ipsa. Repellendus, facere.
        </p>
      </div>
    </div>
  );
}

export default Delivery;
