import React from "react";
import "./moreButton.scss";

function MoreButton() {
  return <a className="main__more-button">Показати більше</a>;
}

export default MoreButton;
